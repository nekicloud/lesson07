//
//  CategoryCellTableViewCell.swift
//  coderswag-app
//
//  Created by luka on 24/12/2018.
//  Copyright © 2018 luka. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    

    func updateViews(category: Category){
        categoryImage.image = UIImage(named: category.imageName)

        categoryTitle.text = category.title
    }

}
