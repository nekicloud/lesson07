//
//  Category.swift
//  coderswag-app
//
//  Created by luka on 25/12/2018.
//  Copyright © 2018 luka. All rights reserved.
//

import Foundation

struct Category{
    private(set) public var title: String
    private(set) public var imageName: String
    
    init(title: String, imageName: String){
      self.title = title
      self.imageName = imageName
    }
}
